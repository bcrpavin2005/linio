<?php

include 'Numero.php';

/**
 * Created by PhpStorm.
 * User: bruno
 * Date: 07/08/17
 * Time: 11:35
 */
$numero = new Numero();
$atual = 0;

while ($atual < 100) {

    //Imprime numero atual
    echo "Atual: " . $atual . "..\n";

    //Testa multiplo de 3
    if ($numero->testaMultiplo($atual, 3)) {
        echo "Linio. É multiplo de 3.\n";
    }

    //Testa Multiplo de 5
    if ($numero->testaMultiplo($atual, 5)) {
        echo "IT. É multiplo de 5. \n";
    }

    //Testa multiplo de 3 e 5
    if ($numero->testaMultiplos($atual, 3, 5)) {
        echo "Linianos...É multiplo de 3 e 5. \n";
    }

    $atual++;

}
echo "FIM..\n";



